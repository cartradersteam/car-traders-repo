package com.akshayn.cartraders;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RatingBar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final RatingBar mRatingBar = new RatingBar(getApplicationContext());
        mRatingBar.setRating(Integer.parseInt("3"));
    }


}
