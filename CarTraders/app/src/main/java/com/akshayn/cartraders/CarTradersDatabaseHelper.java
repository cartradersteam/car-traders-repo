package com.akshayn.cartraders;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Date;


public class CarTradersDatabaseHelper  extends SQLiteOpenHelper {




    public static final String DATABASE_NAME = "cartraders.db";

    public static final String NEWCARS_TABLE = "newcars_table";
    public static final String NEW_ID = "ID";
    public static final String NEW_NAME = "NAME";
    public static final String NEW_PRICE = "PRICE";
    public static final String NEW_COLORS = "COLORS";
    public static final String NEW_TRANSMISSION = "TRANSMISSION";
    public static final String NEW_VERSION = "VERSION";
    public static final String NEW_MILEAGE = "MILEAGE";
    public static final String NEW_FUELTYPE = "FUELTYPE";
    public static final String NEW_EMI = "EMI";
    public static final String NEW_IMAGE = "IMAGE";
    public static final String NEW_RATING = "RATING";


    public static final String OVERVIEW_TABLE = "overview_table";
    public static final String COL_ID = "ID";
    public static final String COL_NAME = "NAME";
    public static final String COL_PRICE = "PRICE";
    public static final String COL_COLOR = "COLOR";
    public static final String COL_KMS = "KMS";
    public static final String COL_MODEL = "MODEL";
    public static final String COL_FUELTYPE = "FUELTYPE";
    public static final String COL_LOCATION = "LOCATION";
    public static final String COL_IMAGE = "IMAGE";
    public static final String COL_YEAR = "YEAR";


    public static final String FEATURES_TABLE = "features_table";
    public static final String FEA_SAFETY = "SAFETY";
    public static final String FEA_BRAKING = "BRAKING";
    public static final String FEA_LOCKING = "LOCKING";
    public static final String FEA_COMFORT = "COMFORT";
    public static final String FEA_STORAGE = "STORAGE";

    public static final String SPECIFICATIONS_TABLE = "specifications_table";
    public static final String SPE_LENGTH = "LENGTH";
    public static final String SPE_WIDTH = "WIDTH";
    public static final String SPE_HEIGHT = "HEIGHT";
    public static final String SPE_CAPACITY = "CAPACITY";
    public static final String SPE_ENGINE = "ENGINE";
    public static final String SPE_WHEELS = "WHEELS";

    public static final String REVIEW_TABLE = "review_table";
    public static final String REV_ID= "ID";
    public static final String CAR_NAME = "CNAME";
    public static final Integer REV_RATING = Integer.valueOf("RATE");
    public static final String REV_REVIEW = "REVIEW";
    public static final String REVIEWER_NAME = "NAAM";
    public static final String REVIEWER_EMAIL = "EMAIL";
    public static final String REV_DATE = "DATE";


    public CarTradersDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + OVERVIEW_TABLE + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME STRING, PRICE STRING, COLORS STRING, TRANSMISSION STRING, VERSION STRING, MILEAGE STRING, FUELTYPE STRING, EMI STRING, IMAGE STRING, RATING INTEGER)");
        Log.d("TAG", "NEW CARS is Created!");

        db.execSQL("create table " + OVERVIEW_TABLE + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME STRING, PRICE STRING, COLOR STRING, KMS INTEGER, MODEL STRING,  FUELTYPE STRING, LOCATION STRING, IMAGE STRING, YEAR INTEGER )");
        Log.d("TAG", "Overview is Created!");


        db.execSQL("create table " + FEATURES_TABLE + " (SAFETY STRING, BRAKING STRING, LOCKING STRING, COMFORT STRING, STORAGE STRING)");
        Log.d("TAG", "Features is Created!");

        db.execSQL("create table " + SPECIFICATIONS_TABLE + " (LENGTH STRING, WIDTH STRING, HEIGHT STRING, CAPACITY STRING, ENGINE STRING, WHEELS STRING)");
        Log.d("TAG", "Specifications is Created!");

        db.execSQL("create table " + REVIEW_TABLE + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, CNAME STRING, RATE STRING, REVIEW STRING, NAAM STRING, EMAIL STRING, DATE DATE )");
        Log.d("TAG", "Review is Created!");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " +NEWCARS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + OVERVIEW_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + FEATURES_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + SPECIFICATIONS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " +REVIEW_TABLE);
        onCreate(db);
    }

    public Cursor getNewCarsData()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor newcars= db.rawQuery("select * from " + NEWCARS_TABLE, null);
        return newcars;
    }

    public Cursor getOverviewData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + OVERVIEW_TABLE, null);
        return res;

    }
    public Cursor getFeaturesData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor fea = db.rawQuery("select * from " + FEATURES_TABLE, null);
        return fea;
    }
    public Cursor getSpecificationsData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor spe = db.rawQuery("select * from " +SPECIFICATIONS_TABLE, null);
        return spe;
    }
    public Cursor getReviewData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor rev = db.rawQuery("select * from " + REVIEW_TABLE , null);
        return rev;

    }


    public boolean insertNewCarsData(String name, String price, String colors, String transmission, String version, String mileage, String fueltype, String emi, String image, Integer rating)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(NEW_NAME, name);
        contentValues.put(NEW_PRICE, price);
        contentValues.put(NEW_COLORS, colors);
        contentValues.put(NEW_TRANSMISSION,transmission);
        contentValues.put(NEW_VERSION,version);
        contentValues.put(NEW_MILEAGE,mileage);
        contentValues.put(NEW_FUELTYPE,fueltype);
        contentValues.put(NEW_EMI,emi);
        contentValues.put(NEW_IMAGE, image);
        contentValues.put(NEW_RATING, rating);

        long newc = db.insert(NEWCARS_TABLE, null, contentValues);

        if (newc==-1)
            return false;
        else
            return true;
    }

    public boolean insertOverviewData(String name, String price, String color, Integer kms, String model,  String fueltype, String location, String image, Integer year) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_NAME, name);
        contentValues.put(COL_PRICE, price);
        contentValues.put(COL_COLOR, color);
        contentValues.put(COL_KMS, kms);
        contentValues.put(COL_MODEL, model);
        contentValues.put(COL_FUELTYPE, fueltype);
        contentValues.put(COL_LOCATION, location);
        contentValues.put(COL_IMAGE, image);
        contentValues.put(COL_YEAR, year);

        long overview = db.insert(OVERVIEW_TABLE, null, contentValues);

        if (overview==-1)
        return false;
        else
            return true;

    }

    public boolean insertFeaturesData(String safety , String braking , String locking, String comfort , String storage)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(FEA_SAFETY, safety);
        contentValues.put(FEA_BRAKING, braking);
        contentValues.put(FEA_LOCKING, locking);
        contentValues.put(FEA_COMFORT, comfort);
        contentValues.put(FEA_STORAGE, storage);

        long features = db.insert(FEATURES_TABLE, null, contentValues);


        if (features == -1)
            return false;
        else
            return true;

}

public boolean insertSpecificationsData(String length, String width ,  String height ,String capacity ,String engine ,String wheels )
{
    SQLiteDatabase db = this.getWritableDatabase();

    ContentValues contentValues = new ContentValues();
    contentValues.put(SPE_LENGTH, length);
    contentValues.put(SPE_WIDTH, width);
    contentValues.put(SPE_HEIGHT, height);
    contentValues.put(SPE_CAPACITY, capacity);
    contentValues.put(SPE_ENGINE, engine);
    contentValues.put(SPE_WHEELS, wheels);

    long specification = db.insert(SPECIFICATIONS_TABLE, null, contentValues);

    if (specification == -1)
        return false;
    else
        return true;
}
    public boolean insertReviewData(String cname, Integer rate, String review, String naam, String email, Date date)

    {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(CAR_NAME, cname);
        contentValues.put(String.valueOf(REV_RATING), rate);
        contentValues.put(REV_REVIEW , review);
        contentValues.put(REVIEWER_NAME, naam);
        contentValues.put(REVIEWER_EMAIL, email);
        contentValues.put(REV_DATE , String.valueOf(date));


        long result = db.insert(REVIEW_TABLE, null, contentValues);

        if (result == -1)
            return false;
        else
            return true;
    }


}